\version "2.18.0"

%%%%%%%%%%% Right Hand %%%%%%%%%%%%%%%

stickRightHand = \relative c' {
     
  \clef treble
%   \set Staff.ottavation = #"7"
  \set Staff.middleCPosition = #1

  \key d \major
  \time 4/4
  %\set Staff.instrumentName = "Melody"
  \set Staff.midiInstrument = "acoustic guitar (steel)"
%   \set fingeringOrientations = #'(right)
%   \override Fingering.color = #blue
%  \tupletSpan 4./

  \repeat volta 2 {
    \mark \default
    r4 r16 <b e g>8 <b d fs>16~ <b d fs>2 |                 % 1, 5
    r4 r16 <a d fs>8 <a~ d e~>16 <a cs e>8 cs16 b cs8 b8 |  % 2, 6
    \break
    r4 r16 <b d a'>8 <b fs'>4~ <b fs'>8. <a cs>8~ |         % 3, 7
  }
  
  \alternative {
    {
      <a cs>1 |                                             % 4
    }
    {
      <a cs>1\repeatTie |                                   % 5, 8
    }
  }
  
  \mark \default
  r4 a16 b16 cs16 d8. cs32 d32 cs16 b8 a16 b16~ |           % 6, 9
  b8 a8 e16 fs16 e8~ e2~ |                                  % 7, 10
  e8 e16 fs16 g16 b16 a16 b4~ b8. a16 a16~ |                % 8, 11
  \break
  a1~ |                                                     % 9, 12
  a4 a16 b16 e4~ e16 d16 e16 fs8. |                         % 10, 13
  \break
  d'16 cs16 a16 e16 fs16 e16 d16 a16~ a2 |                  % 11, 14
  <<
    {  }
    \\
    {  }
    \\
    { b4 a'4 g16 fs16 d16 a16 b16 a16 b16 a16~ }
    \\
    {  }
  >> |                                                      % 12, 15  
  <<
    {  }
    \\
    {  }
    \\
    { a1~ }
    \\
    {  }
  >> |                                                      % 13, 16
  <<
    {  }
    \\
    {  }
    \\
    { <a e'>8 <a e'>8 a'8. e16~ e4 a8. e16~ }
    \\
    {  }
  >> |                                                      % 14, 17
  \break
  <<
    {  }
    \\
    { s4 a,2 a4}
    \\
    { e'4~ e8. a8.~ a8~ a8 e8~ }
    \\
    {  }
  >> |                                                      % 15, 18
  <<
    {  }
    \\
    { r8 a,4 a4 a4. }
    \\
    { e'4 a8. e16 ~ e4 a16(\glissando b16)(\glissando a8~) }
    \\
    {  }
  >> |                                                      % 16, 19
  \break
  <<
    {  }
    \\
    {  }
    \\
    { a8. ~ a4 e8. ~ e4 e8~ }
    \\
    { r4 a,2 a4 }
  >> |                                                      % 17, 20
  <<
    {  }
    \\
    {  }
    \\
    { e'4 a8. e4. e16~ e8~ }
    \\
    { r8 a,4 a4. a4 }
  >> |                                                      % 18 , 21
  \break
  <<
    {  }
    \\
    {  }
    \\
    { e'4~ e8. a8.~ a4 e8~ }
    \\
    { r4 a,2 a4 }
  >> |                                                      % 19, 22
  <<
    {  }
    \\
    {  }
    \\
    { e'4 a8. e16~ e4 a16(\glissando b16)(\glissando a8~) }
    \\
    { r8 a,4 a4 a4. }
  >> |                                                      % 20, 23
  \break
  <<
    {  }
    \\
    {  }
    \\
    { a'8.(\glissando g16) \times 2/3{ gf16 g gf } e16 d4 df16 d16 df b a }
    \\
    {  }
  >> |                                                      % 21, 24
  
}














