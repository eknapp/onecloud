# README #

This is a transcription of the tune "One Cloud" by Rob Martino that is on YouTube here: <https://www.youtube.com/watch?v=_JjimMO2SdE>. I'm doing this transcription with the full approval and permission of Rob Martino. 

This piece has been viewed around 1 million times and has inspired a new generation of Chapman Stick players. Rob did not notate the piece and this transcription work is an attempt to put this important piece into the growing Chapman Stick oeuvre.

### What does this repository contain? ###

This transcription is being done with the Lilypond notation software. <http://www.lilypond.org>. The repository contains the following files.

- **`.gitignore`**: This is the common file so that git ignores certain files.
- **`comp`**: This is a unix script with compiles the scores and MIDI files.
- **`leftHand.ly`**: This is the Lilypond source code for the left hand parts of the piece.
- **`onecloud-articulated.ly`**: This is the Lilypond file which defines how the articulated versions of the score are created. An articulated MIDI file will honor things like repeats and ties for the most part. It's not perfect but it's better than nothing.
- **`onecloud-articulated.midi`**: This is the articulated MIDI file of the piece. If you want to listen to the piece, this is the one to use.
- **`onecloud-articulated.pdf`**: This file is included for completeness but is not the PDF of the score that should be followed. It will not look right.
- **`onecloud.ly`**: This is the Lilypond file which defines how the primary display version of the piece will be created. 
- **`onecloud.midi`**: This is a non-articulated MIDI file version of the piece. It will play exactly like the piece is written while ignoring repeats and other articulations. 
- **`onecloud.pdf`**: This is the PDF that will be the final correct score. Grab this if you want to read the music.
- **`README.md`**: This is the file you are reading now.
- **`rightHand.ly`**: This is the Lilypond source code for the right hand parts of the piece.

### How do I get set up? ###

- To compile this score and MIDI files you will first need to download and install Lilypond. <http://www.lilypond.org>
- If you are on Mac OS X then make the **`comp`** file executable and run it. 
- Linux users will need to modify the script and change the **`open`** command to something compatible with your distro.


### Who do I talk to? ###

* This project is being developed by Eric Knapp, **`eknapp`** here on Bitbucket.
