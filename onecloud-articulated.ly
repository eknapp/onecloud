\version "2.18.0"
\include "english.ly"
\include "articulate.ly"
\include "leftHand.ly"
\include "rightHand.ly"

#(ly:set-option 'point-and-click #f)
#(set-global-staff-size 24)

\paper {
    #(set-paper-size "letter") 
    %ragged-right = ##t
}

\header {
    title = "One Cloud"
    composer = "Rob Martino"
    instrument = "Chapman Stick"
}

\score {
 \articulate <<
    <<
      \set StaffGroup.connectArpeggios = ##t
      \new Staff = melodyStaff <<
          \new Voice = "melody" { \stickRightHand }
      >>
      \new Staff = bassStaff <<
          \new Voice = "stickbass" { \stickLeftHand } 
      >>
    >>
  >>
    \layout {indent = 0.0}
    \midi {
        \tempo 4 = 92
    }
}

