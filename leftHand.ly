\version "2.18.0"

%%%%%%%%%%% Left Hand %%%%%%%%%%%%%%%

stickLeftHand = \relative c, {
          
  \clef bass
%  \set Staff.clefTransposition = #-7
  \set Staff.middleCPosition = #13
  
  \key d \major
  \time 4/4
  %\set #0 #0 Staff.instrumentName = "Bass"
  \set Staff.midiInstrument = "electric bass (pick)"
%   \set fingeringOrientations = #'(right)
%   \override Fingering.color = #blue
  
  \repeat volta 2 {
    %
%     <<
%       {  }
%       \\
%       {  }
%       \\
%       {  }
%       \\
%       {  }
%     >> |
    <<
      {  }
      \\
      { s8. g''16 ~ g4 s8. d16 ~ d4 }
      \\
      { r8. s16 b'4 r8. s16 <d, fs b>4 }
      \\
      { e,2 b4. b16 cs16 }
    >> |                                                    % 1, 5
    <<
      {  }
      \\
      { s8. fs'16 ~ fs4 s8. cs16 ~ cs4 }
      \\
      { r8. s16 a'4 r8. s16 <e a>4 }
      \\
      { d,2 a2 }
    >> |                                                    % 2, 6
    <<
      {  }
      \\
      { s8. b'16 ~ b4 s8. d16 ~ d4 }
      \\
      { r8. s16 g4 r8. s16 <fs b>4 }
      \\
      { g,,4. g16 as16 b4 ~ b16 b8. }
    >> |                                                    % 3, 7
  }
  
  \alternative {
    {
      <<
        {  }
        \\
        { s8. cs'16 ~ cs4 }
        \\
        { r8. s16 <e a>4 }
        \\
        { a,,4. a16 a'8 g16 fs g fs d cs d }
      >> |                                                  % 4
    }
    {
      <<
        {  }
        \\
        { s8. cs'16 ~ cs4 }
        \\
        { r8. s16 <e a>4 }
        \\
        { a,,4. ~ a16 a8 a8 b16 cs16 d16 cs16 b16 }
      >> |                                                  % 5, 8
    }
  }
  <<
    {  }
    \\
    { s8. g''16 ~ g4 s8. d16 ~ d4 }
    \\
    { r8. s16 b'4 r8. s16 <d, fs b>4 }
    \\
    { e,2 b2 }
  >> |                                                      % 6, 9
  <<
    {  }
    \\
    { s8. fs''16 ~ fs4 s8. cs16 ~ cs4 }
    \\
    { r8. s16 a'4 r8. s16 <e a>4 }
    \\
    { d,2 a4 ~ a16 b16 cs16 a16 }
  >> |                                                      % 7, 10
  <<
    {  }
    \\
    { s8. b'16 ~ b4 s8. d16 ~ d4 }
    \\
    { r8. s16 <d g>4 r8. s16 <fs b>4 }
    \\
    { g,,4. g8 b2 }
  >> |                                                      % 8, 11
  <<
    {  }
    \\
    {  }
    \\
    {  }
    \\
    { a4. a16 a'8 fs16 g16 fs16 d16 cs16 d16 e16 ~ }
  >> |                                                      % 9, 12
  <<
    {  }
    \\
    { s8. g'16 ~ g4 s8. d16 ~ d16 }
    \\
    { r8. s16 b'4 r8. s16 <fs b>4 }
    \\
    { e,2 b2 }
  >> |                                                      % 10, 13
  <<
    {  }
    \\
    { s8. fs''16 ~ fs4 s8. cs16 ~ cs4 }
    \\
    { r8. s16 a'4 r8. s16 <e a>4 }
    \\
    { d,2 a2 }
  >> |                                                      % 11, 14
  <<
    {  }
    \\
    { s8. b'16 ~ b4 s8. d16 ~ d4 }
    \\
    { r8. s16 <d g>4 r8. s16 <fs b>4 }
    \\
    { g,,4. g8(\glissando b2) }
  >> |                                                      % 12, 15
  <<
    {  }
    \\
    { s8. cs'16 ~ cs4  }
    \\
    { r8. s16 <e a>4 }
    \\
    { a,,4 ~ a8. a8 as16 b16 cs16 d16 cs16 b cs }
  >> |                                                      % 13, 16
  <<
    {  }
    \\
    { s8. b'16 ~ b4 s8. d16 ~ d4 }
    \\
    { r8. s16 <d g>4 r8. s16 <fs b>4 }
    \\
    { g,,4. g8(\glissando b2) }
  >> |                                                      % 14, 17
  <<
    {  }
    \\
    { s8 cs'8 ~ cs4 s8 cs16 ~ cs16 ~ cs4 }
    \\
    { r8 s8 <e a>4 r8 s16 <e a>16 ~ <e a>4 }
    \\
    { a,,2 ~ a16 a16 ~ a4.}
  >> |                                                      % 15, 18
  <<
    {  }
    \\
    { s8 b'8 ~ b4 s8. d16 ~ d4 }
    \\
    { r8 s8 <d g>4 r8. s16 <fs b>4 }
    \\
    { g,,4. g8(\glissando b2) }
  >> |                                                      % 16, 19
  <<
    {  }
    \\
    { s8 cs'8 ~ cs4 s8 cs16 ~ cs16~ cs4 }
    \\
    { r8 s8 <e a>4 r8 s16 <e a>16~ <e a>4 }
    \\
    { a,,2~ a16 a16~ a4.}
  >> |                                                      % 17, 20
   <<
    {  }
    \\
    { s8 g''8 ~ g4 s8 fs8~ fs4 }
    \\
    { r8 s8 b4 s4 a }
    \\
    { e,4~ e8. a,16 d4. d16 e }
  >> |                                                      % 18, 21
  <<
    {  }
    \\
    { s8 cs'8~ cs4 s8 cs16~ cs16~ cs4 }
    \\
    { r8 s8 <e a>4 r8 s16 <e a>16~ <e a>4 }
    \\
    { a,,2~ a16 a16~ a4. }
  >> |                                                      % 19, 22
  <<
    {  }
    \\
    { s8 b'8 ~ b4 s8. d16 ~ d4 }
    \\
    { r8 s8 <d g>4 r8. s16 <fs b>4 }
    \\
    { g,,4. g8(\glissando b2) }
  >> |                                                      % 20, 23
  <<
    {  }
    \\
    { s8 cs'8~ cs4 s8 cs16~ cs16~ cs4 }
    \\
    { r8 s8 <e a>4 r8 s16 <e a>16~ <e a>4 }
    \\
    { a,,2~ a16 a16~ a4. }
  >> |                                                      % 21, 24
  <<
    {  }
    \\
    {  }
    \\
    {  }
    \\
    {  }
  >> |                                                    % 22, 25
  
}













